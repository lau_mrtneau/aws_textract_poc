import json
import statistics
import sys
from lmu_aws_textract.accessors.aws.aws_s3 import S3
import pandas as pd
from io import StringIO


class TextractParser():
    """ A set of tools to use to transform textract result to the needed format (csv) """

    def __init__(self, textract_response: str) -> None:
        self.textract_response = textract_response
    
    @staticmethod
    def get_text(result, blocks_map):
        text = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                        if word['BlockType'] == 'SELECTION_ELEMENT':
                            if word['SelectionStatus'] =='SELECTED':
                                text +=  'X '    
        return text

    @staticmethod
    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = TextractParser.get_text(cell, blocks_map)
        return rows


    @staticmethod
    def generate_table_csv(table_result, blocks_map, table_index):
        rows = TextractParser.get_rows_columns_map(table_result, blocks_map)

        table_id = 'Table_' + str(table_index)

        # get cells.
        # TO uncomment if necessary
        # csv = 'Table: {0}\n\n'.format(table_id)
        csv = ''

        for row_index, cols in rows.items():

            for col_index, text in cols.items():
                csv += '{}'.format(text) + ";"
            csv += '\n'

        csv += '\n\n\n'
        return csv


    @staticmethod
    def get_table_csv_results(response: json):

        # Get the text blocks
        blocks=response["Blocks"]

        blocks_map={}
        table_blocks=[]

        for block in blocks:
            blocks_map[block["Id"]] = block

            if block["BlockType"] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv_list_table = []
        for index, table in enumerate(table_blocks):
            #csv = ''
            csv = TextractParser.generate_table_csv(table, blocks_map, index +1)
            
            csv_list_table.append(csv)

        return csv_list_table


    @staticmethod
    def textract_parser_csv(textract_response: str, to_csv: bool = False, bucket: str = '', file_name: str = '', s3_path: str = '', aws_profile='') -> None:
        table_csv = TextractParser.get_table_csv_results(textract_response)

        if to_csv:
            for idx, table in enumerate(table_csv):
                output_file = f'output_{idx}.csv'

                with open(output_file, "wt") as fout:
                    fout.write(table)
            
            return None

        else:
            for idx, table in enumerate(table_csv):
                data = StringIO(table)
                table_df = pd.read_csv(data, sep=';')

                key = f'{s3_path}/{idx}_{file_name}'
                table_df.to_csv(f's3://{bucket}/{key}', storage_options={'profile': aws_profile})
