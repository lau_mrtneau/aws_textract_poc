import boto3

boto3 = boto3.Session(profile_name='poc')


def setup_textract_client(region: str) -> boto3.client:
    """ Set the boto3 client for S3 """

    client = boto3.client('textract', region_name=region)

    return client


def setup_s3_client() -> boto3.client:
    """ Set the boto3 client for S3 """
    client = boto3.client('s3')

    return client


def setup_s3_resource() -> boto3.resource:
    """ Set the boto3 client for S3 """
    resource = boto3.resource('s3')

    return resource


def get_file_name(key: str, path_depth: int = None, relative: bool= True) -> str:

    path_listed = key.split("/")

    if path_depth is None:
        return path_listed[-1]

    if path_depth > len(path_listed):
        raise "Path lenth too long !!!!"

    if relative:
        final_key = ""
        for element in range(1, path_depth+1):
            final_key = path_listed[-element] + final_key 
    
        return final_key

    if not relative:
        return path_listed[-path_depth]
