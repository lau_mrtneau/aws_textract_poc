from statistics import mode
from PyPDF2 import PdfFileReader, PdfFileWriter
from io import BytesIO
import random


class Pdf():
    """ A set of tools to work with pdf files """

    @staticmethod
    def load_pdf_reader(s3_resource, bucket_name: str, key: str):
        obj = s3_resource.Object(bucket_name, key)
        fs = obj.get()['Body'].read()

        pdf = PdfFileReader(BytesIO(fs))
      
        return pdf


    def extract_information(s3_resource, bucket_name: str, key: str, log_information: bool):
        pdf = Pdf.load_pdf_reader(s3_resource, bucket_name=bucket_name, key=key)

        information = pdf.getDocumentInfo()

        if log_information:
            info = f"""
                Current file : {bucket_name}/{key}
                Author: {information.author}
                Creator: {information.creator}
                Producer: {information.producer}
                Subject: {information.subject}
                Title: {information.title}
                Number of pages: {pdf.getNumPages()}
            """

        return information


    @staticmethod
    def split_pdf(s3_resource, bucket_name: str, key: str, process_id):
        pdf = Pdf.load_pdf_reader(s3_resource, bucket_name=bucket_name, key=key)
        bucket = s3_resource.Bucket(bucket_name)

        for page in range(pdf.getNumPages()):
            pdf_writer = PdfFileWriter()
            pdf_writer.add_page(pdf.getPage(page))

            filename = f'{key.split("/")[-1].replace(".pdf", "")}_{page}.pdf'

            with BytesIO() as bytes_stream:
                pdf_writer.write(bytes_stream)
                bytes_stream.seek(0)
                bucket.upload_fileobj(bytes_stream, f'splitted/{process_id}/{filename}')
