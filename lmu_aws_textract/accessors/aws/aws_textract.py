import boto3
import json
import logging

logger = logging.getLogger(__name__)

class Textract():
    """ A class to have the necesseray to deal with AWS Textract """
    
    def launch_analyze_document(client: boto3.client, bucket: str, document: str, feature_types: list = ["TABLES", "FORMS"]) -> json:
        """ Return the AWS Textract API result """
        
        try:
            response = client.analyze_document(
                                        Document={'S3Object': {'Bucket': bucket, 'Name': document}},
                                        FeatureTypes=feature_types)

            logger.info(f"Textract detected {len(response['Blocks'])}")
        
        except :
            logger.exception("No text detected.")
            raise

        else:
            return response

