import boto3
import json
import tempfile
import io


class S3():
    """ A class that contains every needs to work with S3 """

    # def __init__(self, profile_name: str = 'default'):
    #     self.profile_name=profile_name
        
    def put_file(client, bucket: str, json_content: json, file_name: str, s3_path: str) -> None:
        """ Load the json content to S3 """

        object_key = f'{s3_path}/{file_name}'

        with tempfile.NamedTemporaryFile() as tmp_file:
            tmp_file.write(json.dumps(json_content).encode("utf-8"))
            client.upload_file(tmp_file.name, bucket, object_key)
            tmp_file.close()

    def load_json(resource, bucket_name: str, object_key: str) -> json:
        """ Return the content of a JSON file """

        data_bytes = resource.Object(bucket_name, object_key).get()['Body'].read()
        decoded_data = data_bytes.decode('utf-8')

        # We work with a string io object
        stringio_data = io.StringIO(decoded_data)

        #We read the content of stringio_data and load json data
        data = stringio_data.readlines()
        json_data = list(map(json.loads, data))

        return json_data