#!/bin/sh

# Date: 09/23/2022
# Author: LMU
# Description: This script create the package to deploy to AWS Lambda


cd .venv/lib/python3.9/site-packages
zip -r ../../../../lambda_package.zip .

cd ../../../../
zip -g lambda_package lambda_function.py
zip -g lambda_package lmu_textract 
