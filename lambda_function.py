from concurrent.futures import process
from lmu_aws_textract.accessors.aws.aws_textract import Textract
from lmu_aws_textract.accessors.aws.aws_s3 import S3
# from lmu_aws_textract.utils.textract_parser_tools import textract_parser_csv
import json
from lmu_aws_textract.utils.textract_parser_tools import TextractParser
import pandas as pd
from lmu_aws_textract.utils.aws_tools import setup_textract_client, setup_s3_client, setup_s3_resource, get_file_name
import logging
from lmu_aws_textract.accessors.pdf.pdf_manager import Pdf
import uuid


textract = setup_textract_client(region = "us-west-2")
s3_client = setup_s3_client()
s3_resource = setup_s3_resource()


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def pre_processing(key: str, process_id: str):
    logging.info('Start pre_processing')

    logging.info('Process the PDF files')
    pdf = Pdf()
    pdf.split_pdf(s3_resource, bucket_name='jsk-lmu-textract', key=key, process_id=process_id)

    response_list_object = s3_client.list_objects_v2(Bucket='jsk-lmu-textract',Prefix=f'splitted/{process_id}/')
    splitted_list = [subresponse['Key'] for subresponse in response_list_object['Contents']]
    
    for object in splitted_list:
        response = Textract.launch_analyze_document(client=textract, bucket='jsk-lmu-textract', document=object)
        S3.put_file(client=s3_client, bucket='jsk-lmu-textract', json_content=response, file_name=f'dev_test.json', 
                                        s3_path=f'textract/extraction/{process_id}/{get_file_name(object, 1)}')
    
    logging.info(f'pre_processing finished for the process id {process_id}.')


def processing(process_id: str):
    response_list_object = s3_client.list_objects_v2(Bucket='jsk-lmu-textract',Prefix=f'textract/extraction/{process_id}/')
    extract_objects_list = [subresponse['Key'] for subresponse in response_list_object['Contents']]

    for object in extract_objects_list:
        json_extraction = S3.load_json(resource=s3_resource, bucket_name='jsk-lmu-textract', object_key=object)
        TextractParser.textract_parser_csv(json_extraction[0], to_csv=False, bucket='jsk-lmu-textract', file_name='result.csv', s3_path=f'textract/processing/{process_id}/{get_file_name(object, 2, relative=False)}', aws_profile='poc')


def main(event: dict):
    key = event['key']
    process_id = uuid.uuid1()

    logging.info(f"Run the process with the id {process_id}")

    pre_processing(key='SC.pdf', process_id=process_id)
    processing(process_id=process_id)
    
    return {
        'statusCode': 200,
        'body': 'Run ok'
    }


if __name__ == '__main__':
    logging.info("Run the process in local !")

    event = {'key': 'SC.pdf'}

    main(event)


