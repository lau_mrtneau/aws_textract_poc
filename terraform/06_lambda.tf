resource "aws_lambda_function" "test_lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = "lambda_function.zip"
  function_name = "lambda_function"
  role          = aws_iam_role.lambda_role.arn
  handler       = "lambda_function.main"

  source_code_hash = filebase64sha256(local.lamdda_package_file)

  runtime = "python3.9"

  environment {
    variables = {
      foo = "bar"
    }
  }
}