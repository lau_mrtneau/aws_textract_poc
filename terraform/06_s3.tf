resource "aws_s3_bucket" "lmu_textract_bucket" {
  bucket = "jsk-lmu-textract"

  tags = {
    Name        = "lmu"
    Environment = "poc"
    Owner       = "laurent.martineau@jellysmack.com"
  }
}


resource "aws_s3_bucket_acl" "lmu_textract_bucket_acl" {
  bucket = aws_s3_bucket.lmu_textract_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "example" {
  bucket = aws_s3_bucket.lmu_textract_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


# resource "aws_s3_bucket_lifecycle_configuration" "lmu_textract_bucket_lifecycle" {
#   bucket = aws_s3_bucket.lmu_textract_bucket.bucket

#   rule {
#     id = "log"

#     filter {
#       and {
#         prefix = "extraction/"
#       }
#     }

#     status = "Enabled"

#     transition {
#       days          = 15
#       storage_class = "STANDARD_IA"
#     }

#     transition {
#       days          = 30
#       storage_class = "GLACIER"
#     }
#   }

#   rule {
#     id = "log"

#     filter {
#       and {
#         prefix = "processing/"
#       }
#     }

#     status = "Enabled"

#     transition {
#       days          = 15
#       storage_class = "STANDARD_IA"
#     }

#     transition {
#       days          = 30
#       storage_class = "GLACIER"
#     }
#   }
# }
