resource "aws_iam_role" "lambda_role" {
  name = "iam_for_lambda"

  assume_role_policy = file("${path.module}/file/lambda_assume_role.json")
}


resource "aws_iam_policy" "lambda_policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = file("${path.module}/file/lambda_assume_role.json")
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}