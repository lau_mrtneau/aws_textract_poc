
# AWS_Textract_POC

A POC project with AWS Textract. 


## Installation

To set up the project you need to create a virtualenv and make the installation with pip 
and the requirements.txt

```bash
  virtualenv .venv
  pip install -r requirements.txt
```
    
## Appendix

https://docs.aws.amazon.com/textract/latest/dg/what-is.html


## Authors

- [@lau_mrtneau](https://gitlab.com/lau_mrtneau)
